import 'package:demo3/enums/status_enum.dart';

class LostItem {
  String category;
  String location;
  DateTime lastSeen = DateTime.now();
  String description;
  String email;
  String phoneNumber;
  String fullName;
  String address;
  StatusEnum status;
  DateTime submitted = DateTime.now();

}
