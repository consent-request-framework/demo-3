import 'dart:async';

import 'package:demo3/enums/status_enum.dart';
import 'package:demo3/models/lost_item.dart';
import 'package:flutter/material.dart';

class LostItemProvider with ChangeNotifier {
  List<LostItem> _items = [];

  List<LostItem> getItems() {
    return [..._items];
  }

  LostItem getByIndex(int index) {
    return _items[index];
  }

  void addItem(LostItem lostItem) {
    lostItem.submitted = DateTime.now();
    lostItem.status = StatusEnum.inProgress;

    _items.add(lostItem);

    Timer(Duration(seconds: 3), () {
      _changeStatus(lostItem);
    });

    notifyListeners();
  }

  void _changeStatus(LostItem lostItem) {
    int countFound =
        getItems().where((x) => x.status == StatusEnum.found).length;
    int countNotFound =
        getItems().where((x) => x.status == StatusEnum.notFound).length;
    lostItem.status =
        countFound < countNotFound ? StatusEnum.found : StatusEnum.notFound;
    notifyListeners();
  }
}
