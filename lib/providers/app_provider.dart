import 'dart:convert';

import 'package:cure_component/models/consent.dart';
import 'package:cure_component/models/data.dart';
import 'package:cure_component/models/purpose.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppProvider with ChangeNotifier {
  int _selectedConsentId;
  List<int> _selectedPurposeIds = [];

  int _previousIndex = 0;
  int _currentIndex = 3;

  List<Consent> _consents = [];
  List<Purpose> _purposes = [];
  List<Data> _data = [];
  List<Data> _activities = [];
  List<Data> _processors = [];

  get navBarAvailable => _currentIndex != 3;

  get currentIndex => _currentIndex;

  set currentIndex(int index) {
    if (_currentIndex == index) {
      return;
    }
    _previousIndex = _currentIndex;
    _currentIndex = index;
    notifyListeners();
  }

  void consentSelected(int consentId, List<int> purposeIds) {
    _selectedConsentId = consentId;
    _selectedPurposeIds = [... purposeIds];
    goToInfo();
  }

  void goBack() {
    currentIndex = _previousIndex;
  }

  void goToInfo() {
    currentIndex = 0;
  }

  void goToFound() {
    currentIndex = 2;
  }

  void goToLost() {
    currentIndex = 1;
  }

  void goToConsent() {
    currentIndex = 3;
  }

  Future<void> fetchAll(ctx) async {
    final data = await rootBundle.loadString('assets/jsons/consent.json');
    final jsonResult = json.decode(data);
    _consents = [];
    for (final item in jsonResult['consents']) {
      _consents.add(Consent.fromMap(item));
    }

    _purposes = [];
    for (final item in jsonResult['purposes']) {
      _purposes.add(Purpose.fromMap(item));
    }

    _data = [];
    for (final item in jsonResult['data']) {
      _data.add(Data.fromMap(item));
    }

    _activities = [];
    for (final item in jsonResult['activities']) {
      _activities.add(Data.fromMap(item));
    }

    _processors = [];
    for (final item in jsonResult['processors']) {
      _processors.add(Data.fromMap(item));
    }

    notifyListeners();
  }

  List<Consent> getConsents() {
    return _consents
        .map((e) => e.copyWith(isSelected: e.id == _selectedConsentId))
        .toList();
  }

  List<Purpose> getPurposes() {
    var selectedConsent;
    if (_selectedConsentId != null) {
      selectedConsent = _consents.firstWhere((e) => e.id == _selectedConsentId);
    }

    var result = [... _purposes.map((e) => e.copyWith()).toList()];
    if (selectedConsent == null || !selectedConsent.isCustomisable) {
      return result;
    }

    return result
        .map((e) => e.copyWith(isSelected: _selectedPurposeIds.contains(e.id)))
        .toList();
  }

  List<Data> getData() {
    return [..._data];
  }

  List<Data> getActivities() {
    return [..._activities];
  }

  List<Data> getProcessors() {
    return [..._processors];
  }

  List<int> getSelectedPurposeIds() {
    return [..._selectedPurposeIds];
  }

  List<Purpose> getAllPurposes() {
    return [..._purposes];
  }
}
