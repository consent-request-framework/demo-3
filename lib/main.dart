import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'apps/consent/consent_app.dart';
import 'apps/found/found_app.dart';
import 'apps/info/info_app.dart';
import 'apps/lost/lost_app.dart';
import 'providers/app_provider.dart';
import 'providers/lost_list_provider.dart';
import 'screens/not_found_screen.dart';
import 'widgets/app_bar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: AppProvider()),
          ChangeNotifierProvider.value(value: LostItemProvider()),
        ],
        child: HomePage(),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  final PageStorageBucket bucket = PageStorageBucket();

  Widget _getPage(int index) {
    switch (index) {
      case 0:
        return InfoApp(key: PageStorageKey('InfoApp'));
      case 1:
        return LostApp(key: PageStorageKey('LostApp'));
      case 2:
        return FoundApp(key: PageStorageKey('FoundApp'));
      case 3:
        return ConsentApp(key: PageStorageKey('ConsentApp'));
    }

    return NotFoundScreen('global');
  }

  @override
  Widget build(BuildContext ctx) {
    return FutureBuilder(
      future: Provider.of<AppProvider>(ctx, listen: false).fetchAll(ctx),
      builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
          ? Center(child: CircularProgressIndicator())
          : Consumer<AppProvider>(
              builder: (ctx, app, ch) => Scaffold(
                body: SafeArea(
                  child: PageStorage(
                    child: _getPage(app.currentIndex),
                    bucket: bucket,
                  ),
                ),
                bottomNavigationBar:
                    app.navBarAvailable ? AppButtonBar() : null,
              ),
            ),
    );
  }
}
