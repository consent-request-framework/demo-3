import 'package:demo3/providers/app_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConsentWidget extends StatelessWidget {
  const ConsentWidget({
    Key key,
    this.widget,
    this.noConsentWidget,
    this.purposeIds,
  }) : super(key: key);

  final Widget widget;
  final Widget noConsentWidget;
  final List<int> purposeIds;

  @override
  Widget build(BuildContext ctx) {
    var app = Provider.of<AppProvider>(ctx, listen: false);
    var expectedPurposeIds = purposeIds == null
        ? app.getAllPurposes().map((e) => e.id)
        : [...purposeIds];
    var selectedPurposeIds = Provider.of<AppProvider>(ctx, listen: false)
        .getSelectedPurposeIds();

    List<int> difference = expectedPurposeIds
        .toSet()
        .difference(selectedPurposeIds.toSet())
        .toList();

    return (difference.length == expectedPurposeIds.length) ? noConsentWidget : widget;
  }
}
