import 'package:demo3/providers/app_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Avatar extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return IconButton(
      icon: Icon(Icons.more_vert),
      onPressed: () {
        showMenu(
          context: ctx,
          position: new RelativeRect.fromLTRB(1.0, 50.0, 0.0, 0.0),
          items: <PopupMenuEntry>[
            PopupMenuItem(
              value: 0,
              child: Container(
                alignment: Alignment.center,
                child: Text('Consent'),
              ),
            ),
          ],
        ).then(
          (value) => {
            if (value == 0)
              {Provider.of<AppProvider>(ctx, listen: false).goToConsent()}
          },
        );
      },
    );
  }
}
