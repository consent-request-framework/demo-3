import 'package:demo3/providers/app_provider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class AppButtonBar extends StatelessWidget {
  const AppButtonBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<AppProvider>(context, listen: false);

    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.infoCircle),
          title: Text('Info'),
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.suitcase),
          title: Text('Lost'),
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.luggageCart),
          title: Text('Found'),
        ),
      ],
      currentIndex: provider.currentIndex,
      onTap: (int value) => {provider.currentIndex = value},
    );
  }
}
