import 'package:demo3/apps/lost/screens/lost_list_screen.dart';
import 'package:demo3/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

import 'screens/lost_item_screen.dart';

class LostApp extends StatelessWidget {
  const LostApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case LostListScreen.routeName:
                return LostListScreen();
              case LostItemScreen.routeName:
                return LostItemScreen();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}
