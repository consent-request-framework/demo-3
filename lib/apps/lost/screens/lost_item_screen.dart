import 'package:demo3/common/consent_widget.dart';
import 'package:demo3/enums/status_enum.dart';
import 'package:demo3/models/lost_item.dart';
import 'package:demo3/providers/lost_list_provider.dart';
import 'package:demo3/widgets/avatar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class LostItemScreen extends StatefulWidget {
  static const routeName = '/submit';

  @override
  _LostItemScreenState createState() => _LostItemScreenState();
}

class _LostItemScreenState extends State<LostItemScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final TextEditingController _lastSeenCtr = new TextEditingController();
  LostItem _lostItem = new LostItem();

  String _validateNotEmpty(String value) {
    return (value == null || value.isEmpty) ? 'Should not be empty!' : null;
  }

  DateTime _convertToDate(String input) {
    return DateFormat.yMd().parseStrict(input);
  }

  String _convertToString(DateTime date) {
    return DateFormat.yMd().format(date);
  }

  Future _chooseDate(BuildContext context) async {
    var now = new DateTime.now();
    var result = await showDatePicker(
        context: context,
        initialDate: now,
        firstDate: new DateTime(1900),
        lastDate: now);

    if (result == null) return;

    setState(() {
      _lastSeenCtr.text = _convertToString(result);
    });
  }

  @override
  Widget build(BuildContext context) {
    var categories = <String>[
      'Documents',
      'Cards',
      'Clothes',
      'Medical devices, medicine',
      'IT devices',
      'Bicycles',
      'Money',
      'Wallets',
      'Musical instruments',
      'Umbrellas',
      'Keys',
      'Jewellery',
      'Bags',
      'Animals',
      'Other'
    ];

    final itemPrd = Provider.of<LostItemProvider>(context, listen: false);
    final int itemIndex = ModalRoute.of(context).settings.arguments;

    categories.sort((a, b) => a.compareTo(b));

    if (itemIndex != null) {
      _lostItem = itemPrd.getByIndex(itemIndex);
      _lastSeenCtr.text = _convertToString(_lostItem.lastSeen);
    }

    Container _getStatusWidget() {
      if (itemIndex == null) {
        return Container();
      }

      String text;
      Color color;
      if (_lostItem.status == StatusEnum.found) {
        text = 'This item has been successfully located.';
        color = Colors.green;
      } else if (_lostItem.status == StatusEnum.notFound) {
        text = 'This item has not been located.';
        color = Colors.red;
      } else if (_lostItem.status == StatusEnum.inProgress) {
        text = 'Search for your item is in progress.';
        color = Colors.orange;
      }

      return Container(
        padding: EdgeInsets.only(top: 20, bottom: 10),
        child: Text(
          text,
          style: TextStyle(
            color: color,
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Report Lost Items'),
        actions: <Widget>[Avatar()],
      ),
      body: Column(
        children: [
          _getStatusWidget(),
          Expanded(
            child: Form(
              key: _formKey,
              child: ListView(
                shrinkWrap: true,
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                children: [
                  SizedBox(height: 10),
                  itemIndex != null
                      ? TextFormField(
                          initialValue: _lostItem.category,
                          readOnly: true,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Category',
                          ),
                        )
                      : DropdownButtonFormField(
                          onSaved: (String value) => _lostItem.category = value,
                          validator: (String value) => _validateNotEmpty(value),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Category',
                          ),
                          items: categories
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          onChanged: (String value) => {},
                          value: _lostItem.category,
                        ),
                  SizedBox(height: 10),
                  TextFormField(
                    onSaved: (String value) => _lostItem.location = value,
                    validator: (String value) => _validateNotEmpty(value),
                    initialValue: _lostItem.location,
                    readOnly: itemIndex != null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Loss location',
                      hintText: 'ZIP code and/or place',
                    ),
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    readOnly: true,
                    validator: (String value) => _validateNotEmpty(value),
                    onSaved: (String value) =>
                        _lostItem.lastSeen = _convertToDate(value),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Last seen on',
                    ),
                    controller: _lastSeenCtr,
                    keyboardType: TextInputType.datetime,
                    onTap: itemIndex == null
                        ? (() {
                            _chooseDate(context);
                          })
                        : null,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    onSaved: (String value) => _lostItem.description = value,
                    readOnly: itemIndex != null,
                    validator: (String value) => _validateNotEmpty(value),
                    initialValue: _lostItem.description,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Lost item description',
                      hintText: 'Lost item description',
                    ),
                  ),
                  SizedBox(height: 10),
                  itemIndex == null
                      ? TextFormField(
                          onSaved: (String value) => _lostItem.fullName = value,
                          readOnly: itemIndex != null,
                          // validator: (String value) => _validateNotEmpty(value),
                          initialValue: _lostItem.fullName,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Full name',
                            hintText: 'Full name',
                          ),
                        )
                      : Container(),
                  SizedBox(height: 10),
                  itemIndex == null
                      ? ConsentWidget(
                          purposeIds: [1],
                          noConsentWidget: Container(
                            padding: EdgeInsets.all(4.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                            ),
                            child: Text(
                              'To receive notifications via email, please provide your consent for data processing in the Consent section of the app.',
                              textAlign: TextAlign.center,
                            ),
                          ),
                          widget: TextFormField(
                            onSaved: (String value) => _lostItem.email = value,
                            readOnly: itemIndex != null,
                            validator: (String value) =>
                                _validateNotEmpty(value),
                            initialValue: _lostItem.email,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Email',
                              hintText: 'Email',
                            ),
                          ),
                        )
                      : Container(),
                  SizedBox(height: 10),
                  itemIndex == null
                      ? ConsentWidget(
                          purposeIds: [2],
                          noConsentWidget: Container(
                            padding: EdgeInsets.all(4.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                            ),
                            child: Text(
                              'To receive notifications via phone, please provide your consent for data processing in the Consent section of the app.',
                              textAlign: TextAlign.center,
                            ),
                          ),
                          widget: TextFormField(
                            onSaved: (String value) =>
                                _lostItem.phoneNumber = value,
                            readOnly: itemIndex != null,
                            // validator: (String value) => _validateNotEmpty(value),
                            initialValue: _lostItem.phoneNumber,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Phone number',
                              hintText: 'Phone number',
                            ),
                          ),
                        )
                      : Container(),
                  SizedBox(height: 10),
                  itemIndex == null
                      ? ConsentWidget(
                          purposeIds: [3],
                          noConsentWidget: Container(
                            padding: EdgeInsets.all(4.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                            ),
                            child: Text(
                              'If you would like us to send an item via post to you, please provide your consent for data processing in the Consent section of the app.',
                              textAlign: TextAlign.center,
                            ),
                          ),
                          widget: TextFormField(
                            onSaved: (String value) =>
                                _lostItem.address = value,
                            readOnly: itemIndex != null,
                            // validator: (String value) => _validateNotEmpty(value),
                            initialValue: _lostItem.address,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Address',
                              hintText: 'Address',
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          (itemIndex == null)
              ? OutlineButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      itemPrd.addItem(_lostItem);
                      Navigator.pop(context);
                    }
                  },
                  child: const Text('Submit'))
              : Container()
        ],
      ),
    );
  }
}
