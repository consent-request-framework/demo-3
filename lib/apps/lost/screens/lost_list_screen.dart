import 'package:demo3/common/consent_widget.dart';
import 'package:demo3/enums/status_enum.dart';
import 'package:demo3/models/lost_item.dart';
import 'package:demo3/providers/lost_list_provider.dart';
import 'package:demo3/widgets/avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'lost_item_screen.dart';

class LostListScreen extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    List<LostItem> _data =
        Provider.of<LostItemProvider>(context, listen: true).getItems();

    Icon _getIcon(int index) {
      final item = _data[index];
      IconData icon;
      Color color;

      if (item.status == StatusEnum.found) {
        icon = Icons.check_circle_outline;
        color = Colors.green;
      } else if (item.status == StatusEnum.notFound) {
        icon = Icons.search_off;
        color = Colors.red;
      } else if (item.status == StatusEnum.inProgress) {
        color = Colors.orange;
        icon = Icons.find_replace;
      }

      return Icon(icon, size: 35, color: color);
    }

    Widget _itemBuilder(BuildContext context, int index) {
      return ListTile(
        leading: Container(
          width: 55,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey.withOpacity(0.2)),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
          ),
          alignment: Alignment.center,
          child: _getIcon(index),
        ),
        title: Text(_data[index].description),
        subtitle: Text(
          'Submitted: ${DateFormat.yMd().format(_data[index].submitted)}',
        ),
        onTap: () {
          Navigator.pushNamed(
            context,
            LostItemScreen.routeName,
            arguments: index,
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Lost Items'),
        actions: <Widget>[Avatar()],
      ),
      body: ConsentWidget(
        purposeIds: [1, 2, 3],
        noConsentWidget: Center(
          child: Text(
            'To process your lost item report we need your consent for personal data processing. Please select the most suitable data processing for you in the Consent section of the app.',
            textAlign: TextAlign.center,
          ),
        ),
        widget: _data.length > 0
            ? ListView.separated(
                padding: const EdgeInsets.all(5),
                itemCount: _data.length,
                itemBuilder: _itemBuilder,
                separatorBuilder: (context, index) {
                  return Divider();
                },
              )
            : Center(
                child: Html(
                  data:
                      '<p style="text-align: center;"><b>No lost items reported.</b></p>',
                ),
              ),
      ),
      floatingActionButton: ConsentWidget(
        purposeIds: [1, 2, 3],
        noConsentWidget: Container(),
        widget: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, LostItemScreen.routeName);
          },
          child: Icon(Icons.add_rounded),
        ),
      ),
    );
  }
}
