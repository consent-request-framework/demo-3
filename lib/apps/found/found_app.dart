import 'package:demo3/screens/not_found_screen.dart';
import 'package:demo3/widgets/avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class FoundApp extends StatelessWidget {
  const FoundApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final htmlContent = """ 
<div style="text-align: center;"><img src="asset:assets/images/found.png"></div>
<h3 style="text-align: center;">Have you found something?</h3>  
<p style="text-align: justify;">Anyone who finds an item that has been lost or forgotten by someone else, has the basic obligation to return it to its rightful owner. Particularly, if the value of the object exceeds 10 Euros and the owner is unidentifiable or if the item is a personal document, the finder has to report the object to the responsible authority. The Lost and Found application will act as a medium for authorities to return the lost items to their owner. </p>

<h3 style="text-align: center;">Are you entitled to a reward?</h3> 
<p style="text-align: justify;">Upon demand, finders are entitled to receive a reward for handing over lost items. The reward value depends on the value of the items, and whether or not it is classified as “lost” or “forgotten”. The reward for lost items is 10% of the value, for forgotten items 5% of the value, and for items with the velue above 2,000 Euro, regardless of lost or forgotten, 50% of the value.</p> 
<p><i>Source:https://www.help.gv.at/Portal.Node/hlpd/public/content/154/Seite.1540000.html</i></p>
     """;

    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case '/':
                return Scaffold(
                  appBar: AppBar(
                    centerTitle: true,
                    actions: <Widget>[Avatar()],
                    title: Text('Found'),
                  ),
                  body: SingleChildScrollView(
                    child: Html(
                      data: htmlContent,
                    ),
                  ),
                );
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}
