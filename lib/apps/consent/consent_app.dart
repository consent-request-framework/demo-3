import 'package:cure_component/cure_component.dart';
import 'package:demo3/providers/app_provider.dart';
import 'package:demo3/screens/not_found_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConsentApp extends StatelessWidget {
  const ConsentApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext ctx) {
            var app = Provider.of<AppProvider>(ctx, listen: false);
            switch (settings.name) {
              case '/':
                return CureComponent(
                  onClose: (consentId, purposeIds) =>
                      {app.consentSelected(consentId, purposeIds)},
                  consents: app.getConsents(),
                  purposes: app.getPurposes(),
                  data: app.getData(),
                  activities: app.getActivities(),
                  processors: app.getProcessors(),
                );
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}
