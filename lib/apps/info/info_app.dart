import 'package:demo3/screens/not_found_screen.dart';
import 'package:demo3/widgets/avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class InfoApp extends StatelessWidget {
  const InfoApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final htmlContent = """ 
<div style="text-align: center;"><img src="asset:assets/images/info.png"></div>
<h3 style="text-align: center;">Have you lost something?</h3>
<h4 style="text-align: center;">Use our Lost &amp; Found app to file a loss report!</h4> 
<p style="text-align: justify;">This application is designed to help you file lost item reports to locate and retrieve lost items in collaboration with the appropriate city authorities. <br/><br/>Lost items fall under the categories of: personal objects, account cards, identity cards, personal documents, and vehicle documents.  <br/><br/>Upon submitting a lost item report, you can track the status on whether or not the item has been found.</p>
<h3 style="text-align: center;">Have you found something?</h3> 
<p style="text-align: justify;">The Lost &amp; Found app also provides further information on helping find the items&#39; rightful owner.</p>
<p><i>Source:https://www.help.gv.at/Portal.Node/hlpd/public/content/154/Seite.1540000.html</i></p>
     """;

    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case '/':
                return Scaffold(
                  appBar: AppBar(
                    centerTitle: true,
                    actions: <Widget>[Avatar()],
                    title: Text('Info'),
                  ),
                  body: SingleChildScrollView(
                    child: Html(
                      data: htmlContent,
                    ),
                  ),
                );
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}
